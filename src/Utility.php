<?php
/**
 * @file
 * Contains \Drupal\contactinfo\Utility
 */
namespace Drupal\contactinfo;
use Drupal\Component\Utility\Html;

class Utility {


 public static function getContactInfoVars($lang = "hu") {
  $contactinfo =  \Drupal::state()->get('contactinfo');
  // Build $variables from scratch.
  if(isset($contactinfo[$lang])){
    $variables['#org'] = isset($contactinfo[$lang]['org']) ? $contactinfo[$lang]['org'] : "";
    $variables['#info'] = isset($contactinfo[$lang]['info']) ? $contactinfo[$lang]['info']['value'] : "";

    // Generates the output for the 'phones' variable.
    if (isset($contactinfo[$lang]['phone'])) {
      $phone_text = $contactinfo[$lang]['phone'];
      $phones = explode(',', $phone_text);
      $x = 0;
      $phones_array = array();
      foreach ($phones as $value) {
        $link_max = strpos($value, '(');

        if($link_max != false){
          $number = substr($value, 0, $link_max);
        }else{
          $number = $value;
        }

        $phones_array[$x]['number'] =  str_replace(" ","",$number);
        $phones_array[$x]['text'] =  $value;
        $x++;
      }
      $variables['#phones'] = $phones_array;
    }

    // Generate the output for the 'email' variable.
    if (isset($contactinfo[$lang]['email'])) {
      $email = $contactinfo[$lang]['email'];
      // Use obfuscation provided by invisimail module.
      if (function_exists('invisimail_encode_html')) {
        $variables['#email'] = invisimail_encode_html($email);
        $variables['#email_url'] = INVISIMAIL_MAILTO_ASCII . $variables[$lang]['email'];
      }
      else {
        $variables['#email'] = $email;
        $variables['#email_url'] = 'mailto:' . $email;
      }
    }
  }
  // Generate ID.
  $id = 'contactinfo';
  $variables['#id'] = Html::getUniqueId($id);
  return $variables;
}

}
