<?php
/**
 * @file
 * Contains \Drupal\contactinfo\Plugin\Block\HeaderContactInfoBlock.
 */

namespace Drupal\contactinfo\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\contactinfo\Utility;

/**
 * Provides a 'Header Contact Info' block.
 *
 * @Block(
 *   id = "headercontactinfo_contact_block",
 *   admin_label = @Translation("Header Contact Info Block")
 * )
 */
class HeaderContactInfoBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $currrent_language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $render = ['#theme' => 'headercontactinfo_block'];
        $render = array_merge($render, Utility::getContactInfoVars($currrent_language));
        $render['#attached']['library'][] = 'contactinfo/contactinfo';
        $render['#cache']['tags'][] = 'contactinfo';
        return $render;
    }
}
