<?php
/**
 * @file
 * Contains \Drupal\contactinfo\Form\ContactInfoSettingsForm
 */

namespace Drupal\contactinfo\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Configure contactinfo settings for this site.
 */
class ContactInfoSettingsForm extends ConfigFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'contactinfo_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $languages = \Drupal::languageManager()->getLanguages();

        /* Az alapértelmezett nyelvet tesszük elsőnek a tömbben */
        foreach ($languages as $lang => $language) {
            if($language->isDefault()){
                $default_language = $language;
            }
        }
        unset($languages[$default_language->getId()]);
        $default_language_with_key_language[$default_language->getId()] = $default_language;
        $all_languages = $default_language_with_key_language + $languages;

        $contactinfo = \Drupal::state()->get('contactinfo');

        foreach ($all_languages as $lang => $language) {

            if($language->isDefault()){
                $text = $this->t('Default language') . " ( " . strtoupper($lang) . " ) ";
                $open = TRUE;
            }else{
                $text = $this->t('Translation') . " ( " . strtoupper($lang) . " ) ";
                $open = FALSE;
            }
            $form['contactinfo_' . $lang] = [
                '#type' => 'details',
                '#open' => $open,
                '#title' => $text,
                '#tree' => TRUE,
            ];
            $form['contactinfo_' . $lang]['org'] = array(
                '#type' => 'textfield',
                '#title' => t('Organization/Business Name'),
                '#default_value' => isset($contactinfo[$lang]['org']) ? $contactinfo[$lang]['org'] : '',
                '#description' => t('The name of your organization or business.'),
                '#prefix' => '<div class="contactinfo-org-wrapper clearfix">',
            );
            $form['contactinfo_' . $lang]['phone'] = array(
                '#type' => 'textfield',
                '#title' => t('Phone Number(s)'),
                '#default_value' => isset($contactinfo[$lang]['phone']) ? $contactinfo[$lang]['phone'] : '',
                '#description' => t('Voice phone numbers, separated by commas. Description could be add in "()" tags. '),
            );
            $form['contactinfo_' . $lang]['email'] = array(
                '#type' => 'textfield',
                '#title' => t('Email'),
                '#default_value' => isset($contactinfo[$lang]['email']) ? $contactinfo[$lang]['email'] : '',
                '#description' => t('Enter this site’s contact email address. This address will be displayed publicly, do not enter a private address.'),
            );
            $moduleHandler = \Drupal::service('module_handler');
            if ($moduleHandler->moduleExists('invismail')) {
                $form['contactinfo_' . $lang]['email']['#description'] .= ' ' . t('This address will be obfuscated to protect it from spammers.');
            } else {
                $form['contactinfo_' . $lang]['email']['#description'] .= ' ' . t('Install the <a href="http://drupal.org/project/invisimail" target="_blank">Invisimail</a> module to protect this address from spammers.');
            }

            $defaults = array(
              'value' => '',
              'format' => filter_default_format(),
            );
            $full_html = isset($contactinfo[$lang]['info']) ? $contactinfo[$lang]['info'] : $defaults;
            $form['contactinfo_' . $lang]['info'] = array(
              '#type' => 'text_format',
              '#format' => $full_html['format'],
              '#title' => t('Information'),
              '#description' => t('Other informations place'),
              '#default_value' => $full_html['value'],
            );
        }

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $all_languages = \Drupal::languageManager()->getLanguages();
        foreach ($all_languages as $lang => $language) {
            $contact_info = $form_state->getValue('contactinfo_'.$lang);
            if (\Drupal::service('email.validator')->isValid($contact_info['email']) == false) {
                $form_state->setErrorByName('contactinfo_'.$lang.'][email', $this->t('The email address is not valid.'));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $all_languages = \Drupal::languageManager()->getLanguages();
        foreach ($all_languages as $lang => $language) {
            $values = $form_state->getValue('contactinfo_'.$lang);
            $lang_settings[$lang]['org'] = $values['org'];
            $lang_settings[$lang]['phone'] = $values['phone'];
            $lang_settings[$lang]['email'] = $values['email'];
            $lang_settings[$lang]['info'] = $values['info'];
        }

        \Drupal::state()->set('contactinfo',$lang_settings);
        Cache::invalidateTags(['contactinfo']);
        parent::submitForm($form, $form_state);
    }
}

